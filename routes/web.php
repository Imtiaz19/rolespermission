<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/',[
    'as'=>'admin.dashboard',
    'uses'=>function(){
    return view('admin.dashboard');
    }
]);
Route::resource('role','RoleController');
Route::resource('assignrole','AssignRoleController');
Route::resource('register','RegisterController');
Route::resource('login','loginController');

//Route::get('/pass/create','RegisterController@password')->name('createPass');
//Route::post('/login','RegisterController@submitPassword')->name('submitPass');
Route::get('/verify/{varification_token}','VerifyController@verify')->name('verify');

