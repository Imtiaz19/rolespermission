<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('user_type');
            $table->string('investment_type');
            $table->string('monthly_payment');
            $table->string('interest_type');
            $table->string('percentage_rate');
            $table->string('issue_date');
            $table->string('target_date_to_collect');
            $table->string('payment_due');
            $table->string('last_payment_installment');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_at');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investments');
    }
}
