<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('mobile')->unique();
            $table->string('email')->unique();
            $table->string('address');
            $table->string('image')->nullable();
            $table->string('password')->nullable();
            $table->string('last_logged_in')->nullable();
            $table->string('last_pass_changed_at')->nullable();
            $table->string('varification_token');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->string('deleted_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
