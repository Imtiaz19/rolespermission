<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyPremium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_premium', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id');
            $table->string('month_of_payment');
            $table->string('total_collected');
            $table->string('fine');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_at');
            $table->timestamps();
            $table->foreign('member_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_premium');
    }
}
