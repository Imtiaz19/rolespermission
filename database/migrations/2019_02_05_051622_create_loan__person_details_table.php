<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanPersonDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan__person_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('address');
            $table->string('email')->unique();
            $table->string('mobile');
            $table->string('document_collected');
            $table->string('loaner_photo');
            $table->string('guarantor_name');
            $table->string('guarantor_phone');
            $table->string('guarantor_photo');
            $table->string('guarantor_email');
            $table->string('document');
            $table->string('loan_amount');
            $table->string('loan_due');
            $table->string('last_loan_installment');
            $table->string('issue_date');
            $table->string('approved_by');
            $table->string('approved_date');
            $table->string('approved_proof');
            $table->string('last_full_payment_date');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('image');
            $table->string('password');
            $table->string('deleted_at');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan__person_details');
    }
}
