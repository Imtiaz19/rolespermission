<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentCollectedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document__collecteds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('user_id');
            $table->string('loan_person_detail_id');
            $table->string('document_file');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_at');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document__collecteds');
    }
}
