<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions=[
            [
                'name' => 'Role-read',
                'display_name' => 'Display role Listing',
                'description' => 'See only Listing Of role'
            ],
            [
                'name' => 'Role-create',
                'display_name' => 'Create role',
                'description' => 'Create New role'
            ],
            [
                'name' => 'Role-edit',
                'display_name' => 'Edit role',
                'description' => 'Edit role'
            ],
            [
                'name' => 'Role-delete',
                'display_name' => 'Delete role',
                'description' => 'Delete role'
            ],
            [
                'name' => 'post-read',
                'display_name' => 'Display Post Listing',
                'description' => 'See only Listing Of Post'
            ],
            [
                'name' => 'post-create',
                'display_name' => 'Create Post',
                'description' => 'Create New Post'
            ],
            [
                'name' => 'post-edit',
                'display_name' => 'Edit Post',
                'description' => 'Edit Post'
            ],
            [
                'name' => 'post-delete',
                'display_name' => 'Delete Post',
                'description' => 'Delete Post'
            ],

            ];
        foreach ($permissions as $key=>$value){
            Permission::create($value);
        }
    }
}
