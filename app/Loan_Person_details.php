<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan_Person_details extends Model
{
    protected $fillable = [
        'full_name','address','email','phone','document_collected','loaner_photo',
        'guarantor_name','guarantor_phone','guarantor_email',
        'guarantor_ photo','document','loan_amount','load_due',
        'last_loan_installment','issue_date','approved_date',
        'last_full_payment_date','approved_by','approved_proof',
        'created_by','updated_by','deleted_at'];
}
