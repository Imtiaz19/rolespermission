<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Notifications\VerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable; use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name','mobile','email','address','varification_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
   /**
    *
    * Returns true if the user is verified
    *
    * */
    public function verified()
    {
        return $this->token === null;
    }
    /**
     *
     * Send the user a verification mail
     *
     * */
    public function sendVerificationEmail()
    {
        $this->notify(new VerifyEmail($this));
    }
    use EntrustUserTrait {
        attachRole as protected entrustAttachRole;
    }
    public function attachRole($role)
    {
        // remove any roles tagged in this user.
        foreach ($this->roles as $userRole) {
            $this->roles()->detach($userRole->id);
        }

        // attach the new role using the `EntrustUserTrait` `attachRole()`
        $this->entrustAttachRole($role);
    }
}
