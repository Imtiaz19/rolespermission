<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monthly_Premium extends Model
{
    protected $fillable = [
        'member_id', 'month_of_payment', 'total_collected','created_by',
        'updated_by','fine','deleted_at',
    ];
    public function member()
    {
        return $this->belongsTo(Users::class,'member_id');
    }
}
