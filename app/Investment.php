<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    protected $fillable = [
        'investment_type','monthly_payment','interest_type','percentage_rate','issue_date','target_date_to_collect','user_type','user_id','payment_due','last_payment_installment','created_by','updated_by','deleted_at',
    ];
    public function user()
    {
        return $this->belongsTo(Users::class);
	}
}
