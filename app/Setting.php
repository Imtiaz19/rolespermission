<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'favicon', 'logo', 'company_name','address','default_fine_amount',
        'default_day_of_month_to_add_fine','created_by','updated_by','deleted_at',
    ];
}
