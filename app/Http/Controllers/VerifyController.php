<?php

namespace App\Http\Controllers;
use App\User;

use Illuminate\Http\Request;

class VerifyController extends Controller
{
    public function verify($token)
    {
        User::where('varification_token',$token)->firstOrFail()
            ->update(['token'=>null]); //verify the user
        return redirect()->route('user.create')->with('success','Account verified');

    }
}
