<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{

    public function index()
    {
        $users = User::all();
        return view('admin.register.usersProfile', compact('users'));
    }


    public function create()

    {
        $roles = Role::all();
        return view('admin.register.registration', compact('roles'));
    }


    public function store(Request $request)
    {    /*validation*/
        $rules = [
            'name' => 'required',
            'mobile' => 'required|unique:users',
            'email' => 'required|unique:users|email',
            'address' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        /*Insert into db*/
        $user = User::create([
            'full_name' => $request['name'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'address' => $request['address'],
            'varification_token' => md5(str_random(25)),

        ]);

        foreach ($request->input('role') as $key => $value) {
            $user->attachRole($value);
        }


        /* Redirect*/

        $user->sendVerificationEmail();
        return redirect()->route('register.index')->with('success', 'User created successfully and verification mail has been sent');
    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        /*get info by user id */
        $users = User::find($id);
        $roles = Role::get();
        $role_user = DB::table("role_user")
            ->where("role_user.user_id", $id)
            ->pluck('role_user.role_id', 'role_user.role_id')->toArray();
        return view('admin.register.editUser', compact(['users', 'roles', 'role_user']));
    }


    public function update(Request $request, $id)
    {
        /*validation*/
        $rules = [
            'name' => 'required',
            'mobile' => 'required',
            'email' => 'required',
            'address' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        //update user info
        $user = User::find($id);
        $user->full_name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->save();
        DB::table('role_user')->where('user_id', $id)->delete();
        foreach ($request->input('role') as $key => $value) {
            $user->attachRole($value);
        }
        /*Redirect*/
        return redirect()->route('register.index')
            ->with('success', 'User Updated successfully');
    }


    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect()->route('register.index')
            ->with('success', 'User Deleted successfully');
    }

}
