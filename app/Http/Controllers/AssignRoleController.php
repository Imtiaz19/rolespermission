<?php

namespace App\Http\Controllers;

use App\AssignRole;
use App\Role;
use Illuminate\Http\Request;

class AssignRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=Role::all();
        return view('admin.role.assignrole',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'roles'=>'required'
        ]);
        $assignrole= new AssignRole([
            'name'=>$request->get('name'),
            'role'=>$request->get('roles'),
        ]);
        $assignrole->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssignRole  $assignRole
     * @return \Illuminate\Http\Response
     */
    public function show(AssignRole $assignRole)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssignRole  $assignRole
     * @return \Illuminate\Http\Response
     */
    public function edit(AssignRole $assignRole)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssignRole  $assignRole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssignRole $assignRole)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssignRole  $assignRole
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssignRole $assignRole)
    {
        //
    }
}
