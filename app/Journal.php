<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    protected $fillable = [
        'amount', 'payment_type', 'payment_id','payment_format','short_description','created_by','updated_by','deleted_at',
    ];
    public function payment()
    {
        return $this->belongsTo(Investment::class,'payment_id');
	}
}
