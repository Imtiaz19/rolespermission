<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document_Collected extends Model
{
    protected $fillable = [
        'type','user_id','loan_person_detail_id','phone','document_file','created_by','updated_by','deleted_at',
    ];
	public function user()
    {
        return $this->belongsTo(Users::class);
	}
	public function loan_person_detail()
    {
        return $this->belongsTo(loan_Person_details::class);
	}
}
