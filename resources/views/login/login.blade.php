@extends('layouts.login')
@section('upper-content')
    <h2>Please Log In </h2>
    @endsection
@section('middle-content')
    <form action="" method="post">
        <label for="email">Email</label>
        <input type="text"  id="email"  class="form-control">
        <br>
        <label for="pass">Password</label>
        <input type="password"  id="pass"   class="form-control">
        <br>
        <input type="submit" class="btn btn-primary" value="Login">
    </form>
@endsection