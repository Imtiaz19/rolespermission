@extends('layouts.dashboard')
@section('content')
    <div class="container">
    <div class="create" style="margin-bottom: 50px">
    <a class="btn btn-success " href="{{route('role.create')}}">Create Role</a>
    </div>
    @if(session('success'))
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <p>{{session('success')}}</p>
        </div>
    @endif

    <table class="table table-dark table-hover">
        <tr>
            <th>Name</th>
            <th>Display Name</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        @forelse($roles as $role)
            <tr >
                <td>{{$role->name}}</td>
                <td>{{$role->display_name}}</td>
                <td>{{$role->description}}</td>
                <td>
                    <a class="btn btn-raised btn-primary btn-sm" href="{{ route('role.edit',$role->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    <form action="{{route('role.destroy',$role->id)}}" id="delete-form-{{ $role->id }}" method="post"  style="display: none;">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}


                    </form>
                    <button onclick="if(confirm('Are you Sure, You went to delete this?')){
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $role->id }}').submit();
                            }else{
                            event.preventDefault();
                            }" class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                </td>
            </tr>
            @empty
        <tr>
            <td>No Roles</td>
        </tr>
            @endforelse


    </table>




</div>





    @endsection