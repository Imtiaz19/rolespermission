@extends('layouts.dashboard')
@section('content')

    <h3 class="text-center">Assign roles</h3>
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <p>{{$error}}</p>
            </div>
        @endforeach
    @endif

    <form action="{{route('assignrole.store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Write a name">
        </div>
        <div class="form-group">
            <label for="roles">Role</label>
            <select name="roles" id="roles" class="form-control">
            <option disabled selected value >Select Role</option>
                @foreach($roles as $role)
            <option value="{{$role->name}}">{{$role->name}}</option>
            @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>


@endsection