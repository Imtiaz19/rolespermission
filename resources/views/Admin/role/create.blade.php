@extends('layouts.dashboard')
@section('content')
    <a href="{{route('role.index')}}" class="btn btn-primary"> Back</a>
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <p>{{$error}}</p>
            </div>
        @endforeach
    @endif
    <h3>Create roles</h3>

    <form action="{{route('role.store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Name of role">
        </div>
        <div class="form-group">
            <label for="display_name">Display Name</label>
            <input type="text" class="form-control" name="display_name" placeholder="Display Name">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" name="description" placeholder="Description of role">
        </div>
        <div class="form-group">
            <label for="permission">Permission</label> <br>
            @foreach($permissions as $permission)
                <input type="checkbox" name="permission[]" value="{{$permission->id}}"> {{$permission->name}} <br>
            @endforeach
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection