@extends('layouts.dashboard')
@section('content')
    <h1 class="text-center">USER REGISTRATION</h1>
    <form action="{{route('register.store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Users Name">
        </div>
        <div class="form-group">
            <label for="mobile">Mobile No</label>
            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Users Mobile No">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Users Email">
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <input type="text" class="form-control" id="address" name="address" placeholder="Users Address">
        </div>
        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" class="form-control" id="image" name="image" placeholder="Users Image">
        </div>

        <button type="submit" class="btn btn-primary">Register</button>
    </form>
@endsection