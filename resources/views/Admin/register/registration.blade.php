@extends('layouts.dashboard')
@section('content')
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <p>{{$error}}</p>
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-{{session('type')}}">
            {{session('message')}}
        </div>
        @endif
  <h1 class="text-center">USER REGISTRATION</h1>
  <form action="{{route('register.store')}}" method="post">
  {{csrf_field()}}
  <div class="form-group">
      <label for="name">Full Name</label>
      <input type="text" class="form-control" id="name" name="name" placeholder="Users Full Name">
  </div>
  <div class="form-group">
      <label for="mobile">Mobile No</label>
      <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Users Mobile No">
  </div>
  <div class="form-group">
      <label for="email">Email</label>
      <input type="text" class="form-control" id="email" name="email" placeholder="Users Email">
  </div>
  <div class="form-group">
      <label for="address">Address</label>
      <input type="text" class="form-control" id="address" name="address" placeholder="Users Address">
  </div>
  <div class="form-group">
      <label for="roll">Roles</label> <br>
      @foreach($roles as $role)
              <input type="checkbox" id="roll" name="role[]" value="{{$role->id}}" > {{$role->name}} <br>
      @endforeach

  </div>
  <button type="submit" class="btn btn-primary">Register</button>
  </form>
@endsection