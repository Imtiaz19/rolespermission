@extends('layouts.dashboard')
@section('content')
    <div class="container">
        <h1 class="text-center">Users Info</h1>
        <br>
        @if(session('success'))
            <div class="alert alert-dismissible alert-success">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <p>{{session('success')}}</p>
            </div>
        @endif

        <table class="table table-dark table-hover">
            <tr>
                <th>Full Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
            @forelse($users as $user)
                <tr>
                    <td>{{$user->full_name}}</td>
                    <td>{{$user->mobile}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->address}}</td>
                    <td>
                        <a class="btn btn-raised btn-primary btn-sm" href="{{ route('register.edit',$user->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <form action="{{route('register.destroy',$user->id)}}" id="delete-form-{{ $user->id }}" method="post"  style="display: none;">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                        </form>
                        <button onclick="if(confirm('Are you Sure, You went to delete this?')){
                                event.preventDefault();
                                document.getElementById('delete-form-{{ $user->id }}').submit();
                                }else{
                                event.preventDefault();
                                }" class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>No Register User</td>
                </tr>
            @endforelse


        </table>




    </div>





@endsection