@extends('layouts.dashboard')
@section('content')
    <a href="{{route('register.index')}}" class="btn btn-primary"> Back</a>
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <p>{{$error}}</p>
            </div>
        @endforeach
    @endif
    <h3>Edit roles</h3>

    <form action="{{route('register.update',$users->id)}}" method="post">
        {{csrf_field()}}
        {{method_field('PATCH')}}
        <div class="form-group">
            <label for="name">Full Name</label>
            <input type="text" value="{{$users->full_name}}" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="mobile">Mobile No</label>
            <input type="text" class="form-control" id="mobile" name="mobile" value="{{$users->mobile}}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" value="{{$users->email}}">
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <input type="text" class="form-control" id="address" name="address" value="{{$users->address}}">
        </div>
        <div class="form-group">
            <label for="roll">Roles</label> <br>
            @foreach($roles as $role)
                <input type="checkbox" {{in_array($role->id,$role_user)?"checked":""}}  id="roll" name="role[]" value="{{$role->id}}" > {{$role->name}} <br>
            @endforeach

        <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
@endsection