@extends('layouts.login')
@section('upper-content')
    <h3>
        Create your pass
    </h3>
    @endsection

@section('middle-content')
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <p>{{$error}}</p>
            </div>
        @endforeach
    @endif
    <form action="{{route('login.store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
        <label for="pass">Password</label>
        <input type="text" name="pass" id="pass"  class="form-control">
        </div>
        <div class="form-group">
        <label for="rpass">Retype Password</label>
        <input type="text"  name="retypepass" id="rpass"   class="form-control">
        </div>
        <br>
        <input type="submit" class="btn btn-primary" value="submit">
    </form>
    @endsection
